# World

# Time

```
date
timestamp
start_time
name
description
seconds_per_timeunit
```

## Map

```sh
name
description
features[]
quests[]
pic80
starting_point # or points
```

### Location

```sh
name
description
items
features[]
tags[] #region name
path_to[]
```

## Quest

```sh
name
description
ActionToDo[]   #pick item, talk to person
reward
```

## Person

```sh
name
description
spieces
sex
age
race
rank
features[]  #skills with levels
storage #bag
```

### View



### Defense

```
defense object can be multiplied
etc..
shield
armor
core
energy
```

### Offense

## Item

```
name
description
value
feature[]
note
```

hidden items TODO

## Action



# Subs

## Player character

## Non-Player character

## Gamemaster